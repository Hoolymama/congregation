/***************************************************************************
matrixGlyphNode.cpp  -  description
-------------------
    begin                : Dec 2009
    copyright            : (C) 2009 by Julian Mann
    email                : julian.mann@gmail.com

    Draw openGL lines showing coord sys
	***************************************************************************/


#include <maya/MVectorArray.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MVector.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MColor.h>

#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnDependencyNode.h>

#include <maya/MDoubleArray.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>

#include <maya/MFnMatrixArrayData.h>
#include <maya/MFnIntArrayData.h>

#include <maya/MFloatVectorArray.h>
#include <maya/MFloatVector.h>
#include "errorMacros.h"


#include "matrixGlyphNode.h"
#include <maya/M3dView.h>

#include "jMayaIds.h"


MObject matrixGlyphNode::aPoints;
MObject matrixGlyphNode::aAxisAngle;
MObject matrixGlyphNode::aFront;
MObject matrixGlyphNode::aUp;
MObject matrixGlyphNode::aFrontAxis;
MObject matrixGlyphNode::aUpAxis;
MObject matrixGlyphNode::aRotateType;
MObject matrixGlyphNode::aLineMult;
MObject matrixGlyphNode::aColorMult;
MObject matrixGlyphNode::aColorOffset;


MObject matrixGlyphNode::aDrawCircles;
MObject matrixGlyphNode::aCircleAxis;
MObject matrixGlyphNode::aCircleColor;
MObject matrixGlyphNode::aCircleScale;

MTypeId matrixGlyphNode::id( k_matrixGlyphNode );
MString matrixGlyphNode::drawDbClassification("drawdb/geometry/matrixGlyphNode");
MString matrixGlyphNode::drawRegistrantId("matrixGlyphPlugin");


matrixGlyphNode::matrixGlyphNode() {}
matrixGlyphNode::~matrixGlyphNode() {}

MStatus matrixGlyphNode::compute( const MPlug &plug, MDataBlock &data )
{
	return MS::kUnknownParameter;
}

void matrixGlyphNode::draw(M3dView &view,
					   const MDagPath &path,
					   M3dView::DisplayStyle style,
					   M3dView::DisplayStatus status)
{
}

bool matrixGlyphNode::isBounded() const
{
	return false;
}

void *matrixGlyphNode::creator()
{
	return new matrixGlyphNode();
}

MStatus matrixGlyphNode::initialize()
{
	MStatus	st;
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnEnumAttribute eAttr;

	aPoints = tAttr.create("points", "pts", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aPoints); mser;


	aAxisAngle = tAttr.create("axisAngle", "axa", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aAxisAngle); mser;


	aFront = tAttr.create("frontVector", "frn", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aFront); mser;


	aUp = tAttr.create("upVector", "up", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setCached(false);
	st = addAttribute(aUp); mser;

	//////////////////
	aRotateType = eAttr.create ("rotateType", "rtp");
	eAttr.addField( "frontUp",		matrixGlyphNode::kFrontUp);
	eAttr.addField( "axisAngle",	 matrixGlyphNode::kAxisAngle );
	eAttr.setDefault(	matrixGlyphNode::kFrontUp);
	eAttr.setHidden(false);
	addAttribute (aRotateType);

	aFrontAxis = eAttr.create ("frontAxis", "fa");
	eAttr.addField( "X",		mayaMath::xAxis);
	eAttr.addField( "Y",	 mayaMath::yAxis );
	eAttr.addField( "Z",	 mayaMath::zAxis );
	eAttr.addField( "negX",		mayaMath::xAxisNeg);
	eAttr.addField( "negY",	 mayaMath::yAxisNeg );
	eAttr.addField( "negZ",	 mayaMath::zAxisNeg );
	eAttr.setDefault(	mayaMath::xAxis);
	eAttr.setHidden(false);
	addAttribute (aFrontAxis);

	aUpAxis = eAttr.create ("upAxis", "ua");
	eAttr.addField( "X",		mayaMath::xAxis);
	eAttr.addField( "Y",	 mayaMath::yAxis );
	eAttr.addField( "Z",	 mayaMath::zAxis );
	eAttr.addField( "negX",		mayaMath::xAxisNeg);
	eAttr.addField( "negY",	 mayaMath::yAxisNeg );
	eAttr.addField( "negZ",	 mayaMath::zAxisNeg );
	eAttr.setDefault(	mayaMath::yAxis);
	eAttr.setHidden(false);
	addAttribute (aUpAxis);
	////////////////////////

	aLineMult = nAttr.create("lineMultiplier", "mlt", MFnNumericData::kFloat);
	nAttr.setDefault( 1.0f );
	nAttr.setKeyable( true );
	st = addAttribute( aLineMult ); mser;

	aColorMult = nAttr.create("colorMult", "cmlt", MFnNumericData::kFloat);
	nAttr.setDefault( 1.0f );
	nAttr.setKeyable( true );
	st = addAttribute( aColorMult ); mser;

	aColorOffset = nAttr.create("colorOffset", "coff", MFnNumericData::kFloat);
	nAttr.setDefault( 0.0f );
	nAttr.setKeyable( true );
	st = addAttribute( aColorOffset ); mser;

	//
	aDrawCircles = nAttr.create("drawCircles", "dcl", MFnNumericData::kBoolean);
	nAttr.setKeyable(true);
	st = addAttribute(aDrawCircles); mser;

	aCircleAxis =  eAttr.create("circleAxis", "cla");
	eAttr.addField( "X",		mayaMath::xAxis);
	eAttr.addField( "Y",	 mayaMath::yAxis );
	eAttr.addField( "Z",	 mayaMath::zAxis );
	eAttr.setDefault(	mayaMath::zAxis);
	eAttr.setKeyable(false);
	addAttribute (aCircleAxis);


	aCircleColor = nAttr.createColor("circleColor", "clc"); 
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	st = addAttribute(aCircleColor);
	mser;

	aCircleScale = nAttr.create("circleScale", "cls", MFnNumericData::kFloat);
	nAttr.setDefault( 0.0f );
	nAttr.setKeyable( true );
	st = addAttribute( aCircleScale ); mser;
	
	return MS::kSuccess;
}
