#ifndef matrixGlyphDrawOverride_H
#define matrixGlyphDrawOverride_H

// Viewport 2.0 includes

#include <stdlib.h>





#include <maya/MDagPath.h>

#include <maya/MDrawRegistry.h>
#include <maya/MPxDrawOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawContext.h>
#include <maya/MSelectionList.h>
#include <maya/MBoundingBox.h>
#include "mayaMath.h"


#include "matrixGlyphDrawData.h"


class matrixGlyphDrawOverride : public MHWRender::MPxDrawOverride {
public:

	static MHWRender::MPxDrawOverride *Creator(const MObject &obj)
	{
		return new matrixGlyphDrawOverride(obj);
	}

	virtual ~matrixGlyphDrawOverride();

	virtual MHWRender::DrawAPI supportedDrawAPIs() const;

	virtual bool isBounded(
	  const MDagPath &objPath,
	  const MDagPath &cameraPath) const;

	virtual MBoundingBox boundingBox(
	  const MDagPath &objPath,
	  const MDagPath &cameraPath) const;

	virtual MUserData *prepareForDraw(
	  const MDagPath &objPath,
	  const MDagPath &cameraPath,
	  const MHWRender::MFrameContext &frameContext,
	  MUserData *oldData);

	virtual bool hasUIDrawables() const { return true; }

	virtual void addUIDrawables(
	  const MDagPath &objPath,
	  MHWRender::MUIDrawManager &drawManager,
	  const MHWRender::MFrameContext &frameContext,
	  const MUserData *data);



	virtual bool traceCallSequence() const
	{
		return false;
	}

	virtual void handleTraceMessage( const MString &message ) const
	{
		MGlobal::displayInfo("matrixGlyphDrawOverride: " + message);
		cerr <<  "matrixGlyphDrawOverride: " << message.asChar() << endl;
	}

private:


void drawCircles(
	MHWRender::MUIDrawManager &drawManager,
	matrixGlyphDrawData *cdata);

	// void drawPoints(MHWRender::MUIDrawManager &drawManager,
	//                 const matrixGlyphDrawData *cdata);

	// void drawLines(MHWRender::MUIDrawManager &drawManager,
	//                const matrixGlyphDrawData *cdata);

 

	matrixGlyphDrawOverride(const MObject &obj);

	static void markDirty(void *clientData);

	matrixGlyphNode  *fNode;

	MCallbackId fModelEditorChangedCbId;
	MCallbackId fTimeChangedCbId;
};


#endif
