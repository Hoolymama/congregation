
#ifndef _glyphNode_H
#define _glyphNode_H

#define cimg_display 0

#include <maya/MPxLocatorNode.h>
#include <maya/MDGMessage.h>
#include <maya/MDGModifier.h>
#include <maya/MVectorArray.h>

#include <maya/MObject.h>
#include <maya/MPxNode.h>
// #include "glyph.h"

// using namespace cimg_library;

class glyphNode : public MPxLocatorNode
{
public:
  glyphNode();
  virtual ~glyphNode();
  static void *creator();
  static MStatus initialize();
  virtual void postConstructor();

  virtual bool isBounded() const;

  virtual MBoundingBox boundingBox() const;

  virtual void draw(
      M3dView &view,
      const MDagPath &path,
      M3dView::DisplayStyle style,
      M3dView::DisplayStatus status);

  MStatus compute(const MPlug &plug, MDataBlock &data);

  static MTypeId id;
  static MString drawDbClassification;
  static MString drawRegistrantId;

  static MObject aPoints;
  static MObject aDirections;

  static MObject aNormalize;
  static MObject aLineMult;
  static MObject aPointSize;

  static MObject aPointColor;
  static MObject aLineColor;

};

#endif
