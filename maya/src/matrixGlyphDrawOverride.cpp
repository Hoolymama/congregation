#include <maya/M3dView.h>
#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MEventMessage.h>
#include "errorMacros.h"

#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>

#include <maya/MFnDependencyNode.h>

#include "matrixGlyphNode.h"
#include "matrixGlyphDrawOverride.h"

matrixGlyphDrawOverride::matrixGlyphDrawOverride(const MObject &obj)
	: MHWRender::MPxDrawOverride(obj, NULL, false)
{

	fModelEditorChangedCbId = MEventMessage::addEventCallback(
		"modelEditorChanged", markDirty, this);

	fTimeChangedCbId = MEventMessage::addEventCallback(
		"timeChanged", markDirty, this);

	MStatus status;
	MFnDependencyNode node(obj, &status);
	fNode = status ? dynamic_cast<matrixGlyphNode *>(node.userNode()) : NULL;
}

matrixGlyphDrawOverride::~matrixGlyphDrawOverride()
{
	fNode = NULL;
	if (fModelEditorChangedCbId != 0)
	{
		MMessage::removeCallback(fModelEditorChangedCbId);
		fModelEditorChangedCbId = 0;
	}

	if (fTimeChangedCbId != 0)
	{
		MMessage::removeCallback(fTimeChangedCbId);
		fTimeChangedCbId = 0;
	}
}

void matrixGlyphDrawOverride::markDirty(void *clientData)
{
	// Mark the node as being dirty so that it can update on display appearance
	// switch among wireframe and shaded.
	matrixGlyphDrawOverride *ovr = static_cast<matrixGlyphDrawOverride *>(clientData);
	if (ovr && ovr->fNode)
	{
		MHWRender::MRenderer::setGeometryDrawDirty(ovr->fNode->thisMObject());
	}
}

MHWRender::DrawAPI matrixGlyphDrawOverride::supportedDrawAPIs() const
{
	// this plugin supports both GL and DX
	return (MHWRender::kOpenGL | MHWRender::kDirectX11 | MHWRender::kOpenGLCoreProfile);
}

bool matrixGlyphDrawOverride::isBounded(const MDagPath &objPath,
										const MDagPath &cameraPath) const
{
	return false;
}

MBoundingBox matrixGlyphDrawOverride::boundingBox(
	const MDagPath &objPath,
	const MDagPath &cameraPath) const
{
	return MBoundingBox();
}

MUserData *matrixGlyphDrawOverride::prepareForDraw(
	const MDagPath &objPath,
	const MDagPath &cameraPath,
	const MHWRender::MFrameContext &frameContext,
	MUserData *oldData)
{
	MStatus st;

	matrixGlyphDrawData *data = dynamic_cast<matrixGlyphDrawData *>(oldData);
	if (!data)
	{
		data = new matrixGlyphDrawData();
	}

	MObject matrixGlyphNodeObj = objPath.node(&st);
	if (st.error())
	{
		return NULL;
	}

	MObject d;
	MPlug(matrixGlyphNodeObj, matrixGlyphNode::aPoints).getValue(d);
	const MVectorArray &positions = MFnVectorArrayData(d).array();
	unsigned len = positions.length();

	float lineMult;
	MPlug(matrixGlyphNodeObj, matrixGlyphNode::aLineMult).getValue(lineMult);

	float colorMult;
	st = MPlug(matrixGlyphNodeObj, matrixGlyphNode::aColorMult).getValue(colorMult);
	mser;

	float colorOffset;
	st = MPlug(matrixGlyphNodeObj, matrixGlyphNode::aColorOffset).getValue(colorOffset);
	mser;

	float highColor = colorMult + colorOffset;
	if (highColor > 1.0f)
	{
		highColor = 1.0f;
	}
	float lowColor = colorOffset;

	data->colorR = MColor(highColor, lowColor, lowColor);
	data->colorG = MColor(lowColor, highColor, lowColor);
	data->colorB = MColor(lowColor, lowColor, highColor);

	short method;
	MPlug(matrixGlyphNodeObj, matrixGlyphNode::aRotateType).getValue(method);

	if (matrixGlyphNode::RotateMethod(method) == matrixGlyphNode::kFrontUp)
	{

		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aFront).getValue(d);
		const MVectorArray &fronts = MFnVectorArrayData(d).array();
		if (fronts.length() != len)
			return NULL;

		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aUp).getValue(d);
		const MVectorArray &ups = MFnVectorArrayData(d).array();
		if (ups.length() != len)
			return NULL;

		short fax;
		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aFrontAxis).getValue(fax);
		mayaMath::axis frontAxis = mayaMath::axis(fax);

		short uax;
		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aUpAxis).getValue(uax);
		mayaMath::axis upAxis = mayaMath::axis(uax);

		data->xLines.setLength(len * 2);
		data->yLines.setLength(len * 2);
		data->zLines.setLength(len * 2);

		MFloatMatrix fmat;
		for (unsigned i = 0; i < len; i++)
		{
			MFloatVector fp = MFloatVector(positions[i]);

			fmat = mayaMath::matFromAim(fp,
										MFloatVector(fronts[i]),
										MFloatVector(ups[i]),
										frontAxis, upAxis);

			unsigned int startIndex = i * 2;
			unsigned int endIndex = startIndex + 1;

			MFloatPoint start = fp;

			data->xLines.set(start, startIndex);
			data->xLines.set((MFloatPoint(lineMult, 0.0f, 0.0f)) * fmat, endIndex);

			data->yLines.set(start, startIndex);
			data->yLines.set((MFloatPoint(0.0f, lineMult, 0.0f)) * fmat, endIndex);

			data->zLines.set(start, startIndex);
			data->zLines.set((MFloatPoint(0.0f, 0.0f, lineMult)) * fmat, endIndex);
		}
	}
	else
	{
		st = MPlug(matrixGlyphNodeObj, matrixGlyphNode::aAxisAngle).getValue(d);
		const MVectorArray &axisAngles = MFnVectorArrayData(d).array();
		if (axisAngles.length() != len)
			return NULL;

		data->xLines.setLength(len * 2);
		data->yLines.setLength(len * 2);
		data->zLines.setLength(len * 2);

		MFloatMatrix fmat;
		for (unsigned i = 0; i < len; i++)
		{
			MFloatVector fp = MFloatVector(positions[i]);
			fmat = mayaMath::matFromPhi(positions[i], axisAngles[i]);

			unsigned int startIndex = i * 2;
			unsigned int endIndex = startIndex + 1;

			MFloatPoint start = fp;

			data->xLines.set(start, startIndex);
			data->xLines.set((MFloatPoint(lineMult, 0.0f, 0.0f)) * fmat, endIndex);

			data->yLines.set(start, startIndex);
			data->yLines.set((MFloatPoint(0.0f, lineMult, 0.0f)) * fmat, endIndex);

			data->zLines.set(start, startIndex);
			data->zLines.set((MFloatPoint(0.0f, 0.0f, lineMult)) * fmat, endIndex);
		}
	}

	MPlug(matrixGlyphNodeObj, matrixGlyphNode::aDrawCircles).getValue(data->doCircles);
	data->circleNormals.clear();
	if (data->doCircles)
	{
		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aCircleColor).child(0).getValue(data->circleColor[0]);
		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aCircleColor).child(1).getValue(data->circleColor[1]);
		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aCircleColor).child(2).getValue(data->circleColor[2]);

		short ca;
		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aCircleAxis).getValue(ca);
		mayaMath::axis circleAxis = mayaMath::axis(ca);

		MPlug(matrixGlyphNodeObj, matrixGlyphNode::aCircleScale).getValue(data->circleScale);
		data->circleNormals.setLength(len);
		if (circleAxis == mayaMath::xAxis)
		{
			for (int i = 0; i < len; i++)
			{
				int j = i * 2;
				data->circleNormals.set(MVector(data->xLines[j + 1] - data->xLines[j]), i);
			}
		}
		else if (circleAxis == mayaMath::yAxis)
		{
			for (int i = 0; i < len; i++)
			{
				int j = i * 2;
				data->circleNormals.set(MVector(data->yLines[j + 1] - data->yLines[j]), i);
			}
		}
		else
		{
			for (int i = 0; i < len; i++)
			{
				int j = i * 2;
				data->circleNormals.set(MVector(data->zLines[j + 1] - data->zLines[j]), i);
			}
		}
	}
	return data;
}

void matrixGlyphDrawOverride::addUIDrawables(
	const MDagPath &objPath,
	MHWRender::MUIDrawManager &drawManager,
	const MHWRender::MFrameContext &context,
	const MUserData *data)
{
	matrixGlyphDrawData *cdata = (matrixGlyphDrawData *)data;
	if (!cdata)
	{
		return;
	}

	drawManager.beginDrawable();
	drawManager.setColor(cdata->colorR);
	drawManager.lineList(cdata->xLines, false);
	drawManager.setColor(cdata->colorG);
	drawManager.lineList(cdata->yLines, false);
	drawManager.setColor(cdata->colorB);
	drawManager.lineList(cdata->zLines, false);

	if (cdata->doCircles)
	{
		drawCircles(drawManager, cdata);
	}
	drawManager.endDrawable();
}

void matrixGlyphDrawOverride::drawCircles(
	MHWRender::MUIDrawManager &drawManager,
	matrixGlyphDrawData *cdata)
{
	int len = cdata->circleNormals.length();
	drawManager.setColor(cdata->circleColor);
	for (unsigned i = 0; i < len; i++)
	{
		drawManager.circle(cdata->xLines[(i * 2)], cdata->circleNormals[i], cdata->circleScale);
	}
}
