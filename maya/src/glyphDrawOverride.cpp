#include <maya/M3dView.h>
#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MEventMessage.h>
#include "errorMacros.h"

#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>

#include "glyphNode.h"
#include "glyphDrawOverride.h"

glyphDrawOverride::glyphDrawOverride(const MObject &obj)
	: MHWRender::MPxDrawOverride(obj, NULL, false)
{

	fModelEditorChangedCbId = MEventMessage::addEventCallback(
		"modelEditorChanged", markDirty, this);

	fTimeChangedCbId = MEventMessage::addEventCallback(
		"timeChanged", markDirty, this);

	MStatus status;
	MFnDependencyNode node(obj, &status);
	fNode = status ? dynamic_cast<glyphNode *>(node.userNode()) : NULL;
}

glyphDrawOverride::~glyphDrawOverride()
{
	fNode = NULL;
	if (fModelEditorChangedCbId != 0)
	{
		MMessage::removeCallback(fModelEditorChangedCbId);
		fModelEditorChangedCbId = 0;
	}

	if (fTimeChangedCbId != 0)
	{
		MMessage::removeCallback(fTimeChangedCbId);
		fTimeChangedCbId = 0;
	}
}

void glyphDrawOverride::markDirty(void *clientData)
{
	// Mark the node as being dirty so that it can update on display appearance
	// switch among wireframe and shaded.
	glyphDrawOverride *ovr = static_cast<glyphDrawOverride *>(clientData);
	if (ovr && ovr->fNode)
	{
		MHWRender::MRenderer::setGeometryDrawDirty(ovr->fNode->thisMObject());
	}
}

MHWRender::DrawAPI glyphDrawOverride::supportedDrawAPIs() const
{
	// this plugin supports both GL and DX
	return (MHWRender::kOpenGL | MHWRender::kDirectX11 | MHWRender::kOpenGLCoreProfile);
}

bool glyphDrawOverride::isBounded(const MDagPath &objPath,
									  const MDagPath &cameraPath) const
{
	return false;
}

MBoundingBox glyphDrawOverride::boundingBox(
	const MDagPath &objPath,
	const MDagPath &cameraPath) const
{
	return MBoundingBox();
}

MUserData *glyphDrawOverride::prepareForDraw(
	const MDagPath &objPath,
	const MDagPath &cameraPath,
	const MHWRender::MFrameContext &frameContext,
	MUserData *oldData)
{
	MStatus st;

	glyphDrawData *data = dynamic_cast<glyphDrawData *>(oldData);
	if (!data)
	{
		data = new glyphDrawData();
	}

	MObject glyphNodeObj = objPath.node(&st);
	if (st.error())
	{
		return NULL;
	}

	MPlug(glyphNodeObj, glyphNode::aPointSize).getValue(data->pointSize);

	MPlug(glyphNodeObj, glyphNode::aNormalize).getValue(data->normalize);

	MPlug(glyphNodeObj, glyphNode::aLineMult).getValue(data->lineMult);


	MPlug(glyphNodeObj, glyphNode::aPointColor).child(0).getValue(data->pointColor[0]);
	MPlug(glyphNodeObj, glyphNode::aPointColor).child(1).getValue(data->pointColor[1]);
	MPlug(glyphNodeObj, glyphNode::aPointColor).child(2).getValue(data->pointColor[2]);

	MPlug(glyphNodeObj, glyphNode::aLineColor).child(0).getValue(data->lineColor[0]);
	MPlug(glyphNodeObj, glyphNode::aLineColor).child(1).getValue(data->lineColor[1]);
	MPlug(glyphNodeObj, glyphNode::aLineColor).child(2).getValue(data->lineColor[2]);

	// Need points for the UIDrawable
	bool doPoints = (data->pointSize > 0.00001);
	bool doLines (data->lineMult > 0.00001);
	if (! (doPoints || doLines)) {
		return data;
	}

	MObject d;

	MPlug(glyphNodeObj, glyphNode::aPoints).getValue(d);
	const MVectorArray &starts  = MFnVectorArrayData(d).array();

	MPlug(glyphNodeObj, glyphNode::aDirections).getValue(d);
	const MVectorArray &directions = MFnVectorArrayData(d).array();

	unsigned num = starts.length();
	data->lineList.clear();
	data->pointList.clear();

	if (doPoints)
	{
		data->pointList.setLength(num);
		for (int i = 0; i < num; ++i)
		{
			data->pointList[i] = starts[i];
		}
	}

	if (! (doLines &&  (directions.length() == starts.length())))
	{
		return data;
	}

	data->lineList.setLength(num * 2);
	for (int i = 0; i < num; ++i)
	{
		unsigned index = i * 2;
		data->lineList[index] = starts[i];
		data->lineList[index + 1] = directions[i];
	}

	if (data->normalize)
	{
		for (int i = 0; i < num; ++i)
		{
			unsigned index = (i * 2) + 1;
			data->lineList[index] = MVector(data->lineList[index]).normal();
		}
	}	

	if (data->lineMult != 1.0f)
	{
		for (int i = 0; i < num; ++i)
		{
			unsigned index = (i * 2) + 1;
			data->lineList[index] = data->lineList[index] * data->lineMult;
		}
	}

	for (int i = 0; i < num; ++i)
	{
		unsigned index = i * 2;
		data->lineList[index] = starts[i];
		data->lineList[index + 1] = data->lineList[index] + data->lineList[index + 1];
	}

	return data;
}

void glyphDrawOverride::addUIDrawables(
	const MDagPath &objPath,
	MHWRender::MUIDrawManager &drawManager,
	const MHWRender::MFrameContext &context,
	const MUserData *data)
{
	glyphDrawData *cdata = (glyphDrawData *)data;
	if (!cdata)
	{
		return;
	}

	if (cdata->pointList.length())
	{
		drawPoints(drawManager, cdata);
	}
	if (cdata->lineList.length())
	{
		drawLines(drawManager, cdata);
	}
}

void glyphDrawOverride::drawPoints(
	MHWRender::MUIDrawManager &drawManager,
	const glyphDrawData *cdata)
{
	drawManager.beginDrawable();
	drawManager.setPointSize(cdata->pointSize);
	drawManager.setColor(cdata->pointColor);
	drawManager.points(cdata->pointList, false);
	drawManager.endDrawable();
}

void glyphDrawOverride::drawLines(
	MHWRender::MUIDrawManager &drawManager,
	const glyphDrawData *cdata)
{

	drawManager.beginDrawable();

	drawManager.setColor(cdata->lineColor);
	drawManager.lineList(cdata->lineList, false);
	drawManager.endDrawable();
}