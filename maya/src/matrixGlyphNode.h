#ifndef _matrixGlyphNode
#define _matrixGlyphNode

#if defined(linux)
#ifndef LINUX
#define LINUX
#endif
#endif

#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <maya/MIOStream.h>

#include <maya/MMatrixArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MPxLocatorNode.h>
#include <maya/MColorArray.h>

#include "mayaMath.h"

class matrixGlyphNode : public MPxLocatorNode
{
public:
	matrixGlyphNode();
	virtual ~matrixGlyphNode();

	virtual MStatus   		compute( const MPlug &plug, MDataBlock &data );

	virtual void            draw( M3dView &view, const MDagPath &path,
	                              M3dView::DisplayStyle style,
	                              M3dView::DisplayStatus status );

	virtual bool            isBounded() const;

	static  void           *creator();
	static  MStatus         initialize();



	static	MTypeId		id;
	static MString drawDbClassification;
	static MString drawRegistrantId;

	static  MObject         aPoints;
	static  MObject         aAxisAngle;
	static  MObject  		aFront;
	static  MObject  		aUp;
	static  MObject  		aFrontAxis;
	static  MObject  		aUpAxis;
	static  MObject  		aRotateType;
	static  MObject         aLineMult;
	static  MObject         aColorMult;
	static  MObject         aColorOffset;

	static  MObject         aDrawCircles;
	static  MObject         aCircleAxis;
	static  MObject         aCircleColor;
	static  MObject         aCircleScale;
	
	

	enum RotateMethod {kFrontUp,  kAxisAngle };

};
#endif

