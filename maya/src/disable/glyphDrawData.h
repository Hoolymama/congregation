


#ifndef glyphDrawData_H
#define glyphDrawData_H



#include <maya/MPxDrawOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawRegistry.h>
#include <maya/MViewport2Renderer.h>
#include <maya/MMatrix.h>
#include <maya/MColor.h>
#include <maya/MPointArray.h>
#include <maya/MColorArray.h>
#include <maya/MIntArray.h>

class glyphDrawData : public MUserData {
public:

	glyphDrawData();
	virtual ~glyphDrawData();

	float pointSize       ;
	MColor color    ;
	MFloatPointArray points;
	MFloatPointArray ends;
	




};



#endif
