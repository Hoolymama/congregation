#include <maya/M3dView.h>
#include <maya/MGlobal.h>
#include <maya/MFnPluginData.h>
#include <maya/MEventMessage.h>
#include "errorMacros.h"

#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnIntArrayData.h>

#include "glyph.h"
#include "glyphDrawOverride.h"


glyphDrawOverride::glyphDrawOverride(const MObject &obj)
	: MHWRender::MPxDrawOverride(obj, NULL, false)
{

	fModelEditorChangedCbId = MEventMessage::addEventCallback(
	                            "modelEditorChanged", markDirty, this);

	fTimeChangedCbId = MEventMessage::addEventCallback(
	                     "timeChanged", markDirty, this);

	MStatus status;
	MFnDependencyNode node(obj, &status);
	fNode = status ? dynamic_cast<glyph *>(node.userNode()) : NULL;

}

glyphDrawOverride::~glyphDrawOverride() {
	fNode = NULL;
	if (fModelEditorChangedCbId != 0)
	{
		MMessage::removeCallback(fModelEditorChangedCbId);
		fModelEditorChangedCbId = 0;
	}

	if (fTimeChangedCbId != 0)
	{
		MMessage::removeCallback(fTimeChangedCbId);
		fTimeChangedCbId = 0;
	}
}

void glyphDrawOverride::markDirty(void *clientData)
{
	// Mark the node as being dirty so that it can update on display appearance
	// switch among wireframe and shaded.
	glyphDrawOverride *ovr = static_cast<glyphDrawOverride *>(clientData);
	if (ovr && ovr->fNode)
	{
		MHWRender::MRenderer::setGeometryDrawDirty(ovr->fNode->thisMObject());
	}
}



MHWRender::DrawAPI glyphDrawOverride::supportedDrawAPIs() const
{
	// this plugin supports both GL and DX
	return (MHWRender::kOpenGL | MHWRender::kDirectX11 | MHWRender::kOpenGLCoreProfile);
}

bool glyphDrawOverride::isBounded( const MDagPath &objPath,
    const MDagPath &cameraPath ) const
{
	return false;
}

MBoundingBox glyphDrawOverride::boundingBox(
  const MDagPath &objPath,
  const MDagPath &cameraPath) const
{
	return MBoundingBox();
}

MUserData *glyphDrawOverride::prepareForDraw(
  const MDagPath &objPath,
  const MDagPath &cameraPath,
  const MHWRender::MFrameContext &frameContext,
  MUserData *oldData
)
{
	MStatus st;

	glyphDrawData *data = dynamic_cast<glyphDrawData *>(oldData);
	if (!data) {
		data = new glyphDrawData();
	}

	MObject glyphObj = objPath.node(&st);
	if (st.error()) {return NULL;}

	MPlug(glyphObj, glyph::aPointSize).getValue(data->pointSize);
 
	MColor color;
	MPlug(glyphObj, glyph::aColor).child(0).getValue(c1[0]);
	MPlug(glyphObj, glyph::aColor).child(1).getValue(c1[1]);
	MPlug(glyphObj, glyph::aColor).child(2).getValue(c1[2]);


	// Need points for the UIDrawable
	MObject d;
	MPlug( glyphObj, glyph::aOutPoints ).getValue(d); mser;
	const MVectorArray &v = MFnVectorArrayData(d).array();
	data->points.clear();
	for (int i = 0; i < v.length(); ++i)
	{
		data->points.append(MPoint(v[i]));
	}

	MPlug( glyphObj, glyph::aOutRadius ).getValue(d); mser;
	data->radius = MFnDoubleArrayData(d).array();

	MPlug( glyphObj, glyph::aOutParams ).getValue(d); mser;
	MDoubleArray params = MFnDoubleArrayData(d).array();

	MPlug( glyphObj, glyph::aOutCounts ).getValue(d); mser;
	data->counts = MFnIntArrayData(d).array();

	MPlug(glyphObj, glyph::aDrawEdges).getValue(data->drawEdges);
	MPlug(glyphObj, glyph::aDrawPoints).getValue(data->drawPoints);
	MPlug(glyphObj, glyph::aDrawCircles).getValue(data->drawCircles);

	bool doRandColor;
	MPlug(glyphObj, glyph::aRandomChainColor).getValue(doRandColor);




	// Color calcs
	data->colors.clear();
	int pIndex = 0;
	unsigned nChains = data->counts.length();
	if (doRandColor) {
		for (int c = 0; c < nChains; ++c)
		{
			MColor randCol(drand48(), drand48(), drand48());
			for (int i = 0; i < data->counts[c]; ++i)
			{
				data->colors.append(randCol);
			}
		}
	}
	else {
		for (int c = 0; c < nChains; ++c)
		{
			for (int i = 0; i < data->counts[c]; ++i)
			{
				float p = params[pIndex];
				float r =  ((c1.r * (1.0 - p)) + (c2.r * p));
				float g =  ((c1.g * (1.0 - p)) + (c2.g * p));
				float b =  ((c1.b * (1.0 - p)) + (c2.b * p));
				data->colors.append(MColor(r, g, b));
				pIndex ++;
			}
		}
	}
	return data;
}


void glyphDrawOverride::addUIDrawables(
  const MDagPath &objPath,
  MHWRender::MUIDrawManager &drawManager,
  const MHWRender::MFrameContext &context,
  const MUserData *data)
{
	glyphDrawData *cdata = (glyphDrawData *)data;
	if (! cdata) { return; }

	if (cdata->drawPoints)
	{
		drawPoints(drawManager, cdata);
	}
	if (cdata->drawEdges)
	{
		drawEdges(drawManager, cdata);
	}
	if (cdata->drawCircles)
	{
		drawCircles(drawManager, cdata);
	}
}

void glyphDrawOverride::drawPoints(
  MHWRender::MUIDrawManager &drawManager,
  const glyphDrawData *cdata)
{
	drawManager.beginDrawable();
	drawManager.setPointSize(cdata->pointSize);
	drawManager.mesh( MHWRender::MUIDrawManager::kPoints , cdata->points , 0,
	                  &(cdata->colors));
	drawManager.endDrawable();
}

void glyphDrawOverride::drawEdges(
  MHWRender::MUIDrawManager &drawManager,
  const glyphDrawData *cdata)
{
	unsigned pIndex = 0;
	drawManager.beginDrawable();
	unsigned nChains = cdata->counts.length();
	MPointArray pts;
	MColorArray cols;
	for (int c = 0; c < nChains; ++c)
	{
		unsigned len =  cdata->counts[c];
		pts.clear();
		cols.clear();
		for (int i = 0; i < len; ++i)
		{
			pts.append(cdata->points[pIndex]);
			cols.append(cdata->colors[pIndex]);
			pIndex++;
		}
		drawManager.mesh( MHWRender::MUIDrawManager::kLineStrip , pts , 0,  &cols);
	}
	drawManager.endDrawable();
}


void glyphDrawOverride::drawCircles(
  MHWRender::MUIDrawManager &drawManager,
  const glyphDrawData *cdata)
{
	drawManager.beginDrawable();
	unsigned len = cdata->points.length();
	for (int i = 0; i < len; ++i)
	{
		drawManager.setColor(cdata->colors[i]);
		drawManager.circle(cdata->points[i], 
		MVector::zAxis, cdata->radius[i]);
	}
	drawManager.endDrawable();
}
