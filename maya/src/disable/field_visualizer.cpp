#include <maya/MIOStream.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MFnMatrixAttribute.h>

#include <maya/MFnMatrixData.h>
#include <maya/MDGMessage.h>
#include <maya/MTime.h>
#include <maya/MMatrix.h>

#include "jMayaIds.h"
#include "field_visualizer.h"

MTypeId fieldVisualizer::id( k_fieldVisualizer ); 

MObject fieldVisualizer::aInMatrix; 	

MObject fieldVisualizer::aDensityX;	
MObject fieldVisualizer::aDensityY;	
MObject fieldVisualizer::aDensityZ;
MObject fieldVisualizer::aDensity;

MObject fieldVisualizer::aDisplayPoints;
MObject fieldVisualizer::aPointSize;
MObject fieldVisualizer::aPointColor;

MObject fieldVisualizer::aDisplaySum;
MObject fieldVisualizer::aSumColor;

MObject fieldVisualizer::aLineSize;
MObject fieldVisualizer::aNormalizeLines;


MObject fieldVisualizer::aFieldVisibility;
MObject fieldVisualizer::aFieldTrigger;
MObject fieldVisualizer::aFieldData;	
MObject fieldVisualizer::aFieldColor; 
MObject fieldVisualizer::aField;
                        
MObject fieldVisualizer::aFieldOutDataPosition;
MObject fieldVisualizer::aFieldOutDataVelocity;
MObject fieldVisualizer::aFieldOutDataMass ;
MObject fieldVisualizer::aFieldOutDataDeltaTime;
MObject fieldVisualizer::aFieldOutData;	
                         
MObject fieldVisualizer::aSamplePositions;	

void *fieldVisualizer::creator()
{
	return new fieldVisualizer();
}

MStatus fieldVisualizer::initialize()
{
	MFnTypedAttribute tAttr;
	MFnEnumAttribute eAttr;
	MFnNumericAttribute nAttr;
	MFnCompoundAttribute cAttr;
	MFnUnitAttribute uAttr;
	MFnMatrixAttribute mAttr;
	MStatus s;

	aInMatrix = mAttr.create( "inMatrix", "imat",  MFnMatrixAttribute::kDouble );
	mAttr.setStorable( false );
	mAttr.setHidden( true );
	addAttribute(aInMatrix);

	aDensityX = nAttr.create( "densityX", "denx",MFnNumericData::kInt);
	nAttr.setMin(2);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(5);

	aDensityY = nAttr.create( "densityY", "deny",MFnNumericData::kInt);
	nAttr.setMin(2);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(5);

	aDensityZ = nAttr.create( "densityZ", "denz",MFnNumericData::kInt);
	nAttr.setMin(2);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(5);

	aDensity = nAttr.create( "density", "den", aDensityX, aDensityY, aDensityZ);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	nAttr.setDefault(5);
	addAttribute(aDensity);

	aDisplayPoints = nAttr.create( "displayPoints", "dps",MFnNumericData::kBoolean);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(true);
	addAttribute(aDisplayPoints );

	aPointSize = nAttr.create( "pointSize", "psi",MFnNumericData::kDouble);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setMin(0.00);
	nAttr.setSoftMax(20.0);
	nAttr.setDefault(5.0);
	addAttribute(aPointSize);	

	aPointColor = nAttr.createColor( "pointColor", "pc");
	nAttr.setStorable(true);
	nAttr.setKeyable(false);
	nAttr.setConnectable(false);
	addAttribute(aPointColor);

	aDisplaySum = nAttr.create( "displaySum", "dsm",MFnNumericData::kBoolean);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(true);
	addAttribute(aDisplaySum );

	aSumColor = nAttr.createColor( "sumColor", "sc");
	nAttr.setStorable(true);
	nAttr.setKeyable(false);
	nAttr.setConnectable(false);
	addAttribute(aSumColor);

	aLineSize = nAttr.create( "lineSize", "ls",MFnNumericData::kDouble);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setMin(0.00);
	nAttr.setSoftMax(20.0);
	nAttr.setDefault(1.0);
	addAttribute(aLineSize);	 

	aNormalizeLines = nAttr.create( "normalizeLines", "nml",MFnNumericData::kBoolean);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(false);
	addAttribute(aNormalizeLines );

	aFieldVisibility = nAttr.create( "fieldVisibility", "fvis",MFnNumericData::kBoolean);
	nAttr.setHidden(false);
	nAttr.setStorable(true);
	nAttr.setReadable(true);
	nAttr.setDefault(true);

	aFieldTrigger = nAttr.create( "fieldTrigger", "ftrg",MFnNumericData::kFloat);
	nAttr.setHidden(false);
	nAttr.setStorable(false);
	nAttr.setReadable(false);
	nAttr.setArray(true);
	nAttr.setIndexMatters(false);

	aFieldColor = nAttr.createColor( "fieldColor", "fc");
	nAttr.setStorable(true);
	nAttr.setKeyable(true);

	aFieldData = tAttr.create("fieldData", "fid", MFnData::kVectorArray); 
	tAttr.setStorable(false);
	tAttr.setReadable( false );

	aField  = cAttr.create( "field", "f", &s);
	cAttr.setArray( true );
	cAttr.addChild(aFieldData);
	cAttr.addChild(aFieldVisibility);
	cAttr.addChild(aFieldColor);
	cAttr.addChild(aFieldTrigger);	
	s = addAttribute( aField);

	aFieldOutDataPosition = tAttr.create("fieldOutDataPosition", "fodp", MFnData::kVectorArray); 
	tAttr.setStorable(false);
	tAttr.setReadable(true);
	addAttribute(aFieldOutDataPosition);

	aFieldOutDataVelocity = tAttr.create("fieldOutDataVelocity", "fodv", MFnData::kVectorArray); 
	tAttr.setStorable(false);
	tAttr.setReadable(true);    
	addAttribute(aFieldOutDataVelocity);

	aFieldOutDataMass = tAttr.create("fieldOutDataMass", "fodm", MFnData::kDoubleArray); 
	tAttr.setStorable(false);
	tAttr.setReadable(true);    
	addAttribute(aFieldOutDataMass);

	aFieldOutDataDeltaTime = uAttr.create("fieldOutDataDeltaTime", "foddt", MFnUnitAttribute::kTime,0.0);
	uAttr.setReadable(true);	
	uAttr.setStorable(false);
	s = addAttribute(aFieldOutDataDeltaTime);

	aFieldOutData  = cAttr.create( "fieldOutData", "fod", &s);
	cAttr.addChild(aFieldOutDataPosition);
	cAttr.addChild(aFieldOutDataVelocity);
	cAttr.addChild(aFieldOutDataMass);
	cAttr.addChild(aFieldOutDataDeltaTime);
	s = addAttribute( aFieldOutData);

	aSamplePositions = tAttr.create("samplePositions", "sp", MFnData::kVectorArray); 
	tAttr.setStorable(false);
	tAttr.setWritable(false);
	tAttr.setReadable(true);
	addAttribute(aSamplePositions);	

	attributeAffects(aDensityX,aFieldOutData);
	attributeAffects(aDensityX,aFieldOutDataPosition);
	attributeAffects(aDensityX,aFieldOutDataVelocity);
	attributeAffects(aDensityX,aFieldOutDataMass);
	attributeAffects(aDensityX,aSamplePositions);

	attributeAffects(aDensityY,aFieldOutData);
	attributeAffects(aDensityY,aFieldOutDataPosition);
	attributeAffects(aDensityY,aFieldOutDataVelocity);
	attributeAffects(aDensityY,aFieldOutDataMass);
	attributeAffects(aDensityY,aSamplePositions);

	attributeAffects(aDensityZ,aFieldOutData);
	attributeAffects(aDensityZ,aFieldOutDataPosition);
	attributeAffects(aDensityZ,aFieldOutDataVelocity);
	attributeAffects(aDensityZ,aFieldOutDataMass);	
	attributeAffects(aDensityZ,aSamplePositions);

	attributeAffects(aDensity,aFieldOutData);
	attributeAffects(aDensity,aFieldOutDataPosition);
	attributeAffects(aDensity,aFieldOutDataVelocity);
	attributeAffects(aDensity,aFieldOutDataMass);	
	attributeAffects(aDensity,aSamplePositions);

	attributeAffects(aInMatrix,aFieldOutData);	
	attributeAffects(aInMatrix,aFieldOutDataPosition);	
	attributeAffects(aInMatrix,aFieldOutDataVelocity);
	attributeAffects(aInMatrix,aFieldOutDataMass);	
	attributeAffects(aInMatrix,aSamplePositions);

	attributeAffects(aFieldTrigger,aFieldOutData);
	attributeAffects(aFieldTrigger,aFieldOutDataPosition);
	attributeAffects(aFieldTrigger,aFieldOutDataVelocity);
	attributeAffects(aFieldTrigger,aFieldOutDataMass);

	attributeAffects(aFieldTrigger,aSamplePositions);

	return MS::kSuccess;
}

fieldVisualizer::fieldVisualizer() {}

fieldVisualizer::~fieldVisualizer() {}

inline MStatus vectorArrayFromPlug(
	MDataBlock& data,
	const MObject &att, 
	MVectorArray &arr 
){
	MStatus st;
	MDataHandle h = data.inputValue(att, &st);
	if (st!=MS::kSuccess) return st;
	MObject o = h.data();						
	if (o.hasFn(MFn::kVectorArrayData))  {		
		MFnVectorArrayData fn(o);				
		arr.copy( fn.array( &st ));				
	}
	return st;
}

// project a vector into the plane described by a normal
//inline MVector projVect( MVector proj, MVector norm ) { return ( proj - ( ( proj * norm ) * norm ) ); };


void fieldVisualizer::draw(	M3dView & view,
	const MDagPath & path,
	M3dView::DisplayStyle style,
	M3dView:: DisplayStatus	status	)
{

	MStatus st;

	MObject thisObj = thisMObject();

	// get sample positions from output
	MPlug positionPlug(thisObj, aSamplePositions);
	MObject dPositions;
	st = positionPlug.getValue(dPositions); mser;
	MFnVectorArrayData fnV(dPositions);
	MVectorArray position = fnV.array(&st); mser;

	unsigned pl =position.length();

	view.beginGL();

	MPlug scalePlug (thisObj,aLineSize);
	double lineSize; 
	scalePlug.getValue(lineSize);

	if (lineSize == 0.0) return; 






	bool doSamplePos; 
	MPlug(thisObj,aDisplayPoints).getValue(doSamplePos);

	if (doSamplePos)
	{
		MPlug pointSizePlug (thisObj,aPointSize);
		double pointSize; 
		pointSizePlug.getValue(pointSize);

		MPlug colorPlug (thisObj,aPointColor);
		float colorRed,colorGreen,colorBlue;
		colorPlug.child(0).getValue(colorRed);
		colorPlug.child(1).getValue(colorGreen);
		colorPlug.child(2).getValue(colorBlue);
		view.setDrawColor( MColor( MColor::kRGB, colorRed, colorGreen, colorBlue ) );

		glPushAttrib(GL_CURRENT_BIT);
		glPointSize(float(pointSize));

		glBegin( GL_POINTS );

		for(unsigned i=0;i<pl;i++){
			glVertex3f(float(position[i].x),float(position[i].y),float(position[i].z));
		}
		glEnd();
		glPopAttrib();
	}

	MPlug normPlug(thisObj,aNormalizeLines);
	bool doNorm;
	normPlug.getValue(doNorm);



	MPlug sumPlug(thisObj,aDisplaySum);
	bool doSum;
	MVectorArray sum;
	sumPlug.getValue(doSum);
	if (doSum) {
		sum = MVectorArray(pl, MVector::zero);
	}


	MFnMatrixData fnX;
	MObject d;
	MPlug plug( thisObj, aInMatrix );
	st = plug.getValue(d);
	MMatrix mat;
	if (st.error()) {
		mat.setToIdentity();
	} else {
		fnX.setObject(d);
		mat = fnX.matrix(&st);
		if (st.error()) {
			mat.setToIdentity();
		} 
	}
	MMatrix imat = mat.inverse();


	glPushAttrib(GL_CURRENT_BIT);
	glBegin( GL_LINES );

	// get the plug to the field
	MPlug fieldPlug(thisObj, aField);
	unsigned numFields = fieldPlug.numElements(&st);mser;
	// cerr << "numFields: " << numFields << endl;

	// loop through all multi elements



	int realFieldCount = 0;
	for (unsigned nf = 0; nf<numFields; nf++)
	{

		MPlug tmpFieldPlug = fieldPlug.elementByPhysicalIndex(nf,&st);
		if (st.error()){
			//cerr <<"\ncan't access field element";
			continue;
		}

		MPlug visPlug = tmpFieldPlug.child(aFieldVisibility,&st);
		bool vis;
		visPlug.getValue(vis);
		if (!vis) continue;

		// get field element
		MPlug fieldDataPlug = tmpFieldPlug.child(aFieldData,&st);
		if (st.error()){
			//cerr <<"\ncan't access fieldDataPlug";
			continue;
		}

		if (fieldDataPlug.isConnected()) 
		{

			MObject fieldDataObject;
			st = fieldDataPlug.getValue(fieldDataObject); 
			if (st.error()){
				// cerr <<"\ncan't access fieldData value";
				continue;
			}

			MFnVectorArrayData tmpFieldDataFn(fieldDataObject);
			MVectorArray tmpFieldData = tmpFieldDataFn.array(&st);

			if (st.error()){
				// cerr <<"\ncan't access fieldData array";
				continue;
			}

			if (tmpFieldData.length() == pl) 
			{

				// get color
				MPlug colorPlug = tmpFieldPlug.child(aFieldColor,&st);
				float colorRed,colorGreen,colorBlue;
				colorPlug.child(0).getValue(colorRed);
				colorPlug.child(1).getValue(colorGreen);
				colorPlug.child(2).getValue(colorBlue);
				view.setDrawColor( 
					MColor( MColor::kRGB, colorRed, colorGreen, colorBlue ) 
				);

				MVectorArray dirs(pl, MVector::zero);
				if (doNorm)
				{
					for (unsigned i=0; i < pl; i++)
					{	
						// dir.normalize();
						dirs[i] =  (tmpFieldData[i].normal() * lineSize) * imat;
						MVector dir = dirs[i] + position[i];

						glVertex3f(float (position[i].x),float (position[i].y),float (position[i].z));
						glVertex3f(float (dir.x),float (dir.y),float (dir.z));
					}	
								
				}
				else
				{
					for (unsigned i=0; i < pl; i++)
					{	
						dirs[i] = (tmpFieldData[i] * lineSize)  * imat;
						MVector dir = dirs[i] + position[i];

						glVertex3f(float (position[i].x),float (position[i].y),float (position[i].z));
						glVertex3f(float (dir.x),float (dir.y),float (dir.z));
					}
				}

				if (doSum) {
					for (unsigned i=0; i < pl; i++) {
						sum[i] += dirs[i];
					}
				}
				realFieldCount++;
			}
			// else{
			// 	cerr <<"\nlength of fieldData "<< tmpFieldData.length() <<"does not match output "<<pl;	
			// }
		}
	}


	if ((realFieldCount > 1) && (doSum)) {
		// get color
		MPlug colorPlug (thisObj,aSumColor);
		float colorRed,colorGreen,colorBlue;
		colorPlug.child(0).getValue(colorRed);
		colorPlug.child(1).getValue(colorGreen);
		colorPlug.child(2).getValue(colorBlue);
		view.setDrawColor( MColor( MColor::kRGB, colorRed, colorGreen, colorBlue ) );


		for (unsigned i=0; i < pl; i++) {
			sum[i] += position[i];
			glVertex3f(float (position[i].x),float (position[i].y),float (position[i].z));
			glVertex3f(float (sum[i].x),float (sum[i].y),float (sum[i].z));
		}
	}

	glEnd();
	glPopAttrib();

	view.endGL();

}

MStatus fieldVisualizer::compute( const MPlug& plug, MDataBlock& data )
{

	MStatus st;

	if(!(
		( plug == aFieldOutData) ||
		( plug == aFieldOutDataPosition) ||
		( plug == aSamplePositions)
		)) return MS::kUnknownParameter;	

	// get all trigger values to force update
	/////////////////////////////////////
	float dummy;
	MArrayDataHandle hField = data.inputArrayValue(aField);
	unsigned nf = hField.elementCount();
	for(unsigned i = 0;i < nf; i++, hField.next()) {
		MDataHandle hEl = hField.inputValue();
		MArrayDataHandle hTrigger = hEl.child(aFieldTrigger);
		unsigned nt = hTrigger.elementCount();
		for(unsigned j = 0;j < nt; j++, hTrigger.next()) {
			MDataHandle hTriggerEl = hTrigger.inputValue();
			dummy = hTrigger.inputValue().asFloat();
		}
	}
	/////////////////////////////////////

	// Strategy:
	// Create a grid of n points in -0.5 to 0.5
	// Find out where these points are in WS (using inMatrix) 
	// Sample the fields in WS.
	// Get the force vectors back from fields and bring them 
	// into local space.
	// Draw, points and vectors.

	int densityX = data.inputValue(aDensityX).asInt();
	int densityY = data.inputValue(aDensityY).asInt();
	int densityZ = data.inputValue(aDensityZ).asInt();

	if (densityX < 2) densityX = 2;
	if (densityY < 2) densityY = 2;
	if (densityZ < 2) densityZ = 2;

	int count = densityX * densityY * densityZ;

	float xSpacing = 1.0 / float(densityX-1);
	float ySpacing = 1.0 / float(densityY-1);
	float zSpacing = 1.0 / float(densityZ-1);

	// make outputs
	MVectorArray position(count,MVector::zero);
	MVectorArray velocity(count,MVector::zero);
	MDoubleArray mass(count,1.0);
	MTime deltaTime(1.0, MTime::uiUnit());

	int i=0;

	// generate local sample positions
	for (int x=0; x < densityX; x++)
	{
		for (int y=0; y < densityY; y++)
		{
			for (int z=0; z < densityZ; z++)
			{
				position[i] = MVector( 
					(-0.5+(x*xSpacing)) ,
					(-0.5+(y*ySpacing)) ,
					(-0.5+(z*zSpacing)));
				i++;							
			}
		}
	}

	// set the sample points on output
	MDataHandle hPosition = data.outputValue(aSamplePositions);
	MFnVectorArrayData fnV;
	MObject dSamplePositions = fnV.create( position, &st );
	hPosition.set(dSamplePositions);
	st= data.setClean(aSamplePositions);	

	if ( plug == aSamplePositions) return MS::kSuccess;

	MMatrix wm = data.inputValue(aInMatrix).asMatrix(); msert;

	MVectorArray fieldOutDataPosition(count,MVector::zero);
	for(int i=0;i< count; i++){
		fieldOutDataPosition[i] = MVector( MPoint(position[i]) * wm);
	}

	MDataHandle hFieldOutData = data.outputValue(aFieldOutData);
	MDataHandle hFieldOutDataPosition  = hFieldOutData.child(aFieldOutDataPosition);
	MDataHandle hFieldOutDataVelocity  = hFieldOutData.child(aFieldOutDataVelocity);
	MDataHandle hFieldOutDataMass 	   = hFieldOutData.child(aFieldOutDataMass);
	MDataHandle hFieldOutDataDeltaTime = hFieldOutData.child(aFieldOutDataDeltaTime);

	// create objects
	MFnDoubleArrayData fnDoubleData;
	MObject dFieldOutDataPosition = fnV.create( fieldOutDataPosition, &st );mser;
	MObject dFieldOutDataVelocity = fnV.create( velocity, &st );mser;
	MObject dFieldOutDataMass = fnDoubleData.create( mass, &st );mser;

	// update data block with new output data.
	hFieldOutDataPosition.set(dFieldOutDataPosition);
	hFieldOutDataVelocity.set(dFieldOutDataVelocity);
	hFieldOutDataMass.set(dFieldOutDataMass);
	hFieldOutDataDeltaTime.set(MTime(1.0));

	st= data.setClean(aFieldOutDataPosition);mser;
	data.setClean(aFieldOutDataVelocity);
	data.setClean(aFieldOutDataMass);
	data.setClean(aFieldOutDataDeltaTime);
	st=data.setClean(aFieldOutData);mser;
   	
	return MS::kSuccess;	
}


void fieldVisualizer::postConstructor()
{
	MFnDependencyNode nodeFn(thisMObject());
	nodeFn.setName("fieldVisualizerShape#");
	setExistWithoutInConnections(true);
	setExistWithoutOutConnections(true);
}

bool fieldVisualizer::isBounded() const
{ 
	return true;
}

MBoundingBox fieldVisualizer::boundingBox() const
{   
	return MBoundingBox(MPoint(-0.5,-0.5,-0.5),MPoint(0.5,0.5,0.5));
}
