
#ifndef _FIELD_VISUALIZER_H
#define _FIELD_VISUALIZER_H
#include <maya/MIOStream.h>
// #include <iostream.h>
#include <math.h>

#include <maya/MPxLocatorNode.h>
#include <maya/MObject.h>
#include <maya/M3dView.h>
#include <maya/MTypeId.h>
// #include <maya/MFnPlugin.h>
#include <maya/MPlug.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MPoint.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MSelectionList.h>
#include <maya/MGlobal.h>
#include <maya/MString.h>
#include <maya/MStringArray.h>

#include <maya/MDagPath.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MDGModifier.h>

#include <maya/MDGMessage.h>
#include "errorMacros.h"




class fieldVisualizer : public MPxLocatorNode
{

	public :
	enum vectorType
	{
		kVtForce,
		kVtDirection
	};

	fieldVisualizer();
	virtual ~fieldVisualizer();

	static void *creator();
	static MStatus initialize();
	virtual void postConstructor();
	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const; 


	virtual void draw(	M3dView & view,
		const MDagPath & path,
		M3dView::DisplayStyle style,
		M3dView:: DisplayStatus	status	);

	virtual	MStatus	compute(const MPlug& plug, MDataBlock& data);


	static MTypeId id;



	static MObject aInMatrix; 	

	static MObject aDensityX;	
	static MObject aDensityY;	
	static MObject aDensityZ;
	static MObject aDensity;
	static MObject aDisplayPoints;

	static MObject aPointSize;
	static MObject aPointColor;


	static MObject aDisplaySum;
	static MObject aSumColor;


	static MObject aLineSize;
	static MObject aNormalizeLines;

	static MObject aFieldVisibility;
	static MObject aFieldTrigger;
	static MObject aFieldData;	
	static MObject aFieldColor; 
	static MObject aField;

	static MObject aFieldOutDataPosition;
	static MObject aFieldOutDataVelocity;
	static MObject aFieldOutDataMass ;
	static MObject aFieldOutDataDeltaTime;
	static MObject aFieldOutData;	

	static MObject aSamplePositions;	



	;

protected:

};

namespace fieldVisualizerCallback
{
	static 	MCallbackId	id;

	static void makeDefaultConnections(  MObject & node, void* clientData )
	{

		MPlug wmPlugmulti( node, fieldVisualizer::worldMatrix );
		MPlug wm( wmPlugmulti.elementByLogicalIndex( 0 ) );
		MPlug mt( node, fieldVisualizer::aInMatrix );

		MDGModifier mod;	
		mod.connect( wm, mt );
		MStatus stat = mod.doIt();
		if (stat != MS::kSuccess)
			stat.perror("fieldVisualizer ERROR :: callback unable to make connections");		
	}
}


#endif




















