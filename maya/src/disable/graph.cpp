/*
 *  graph.cpp
 *  jtools
 *
 *  Created by Julian Mann on 28/12/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

/*
 *  graph.cpp
 *  animal
 *
 *  Created by Julian Mann on 30/07/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */




// #include <maya/MIOStream.h>
#include <math.h>

#include <maya/MPlug.h>

#include <maya/MVectorArray.h>

#include <maya/MIntArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>

#include <maya/MColor.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnGenericAttribute.h>
#include <maya/MDoubleArray.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnDependencyNode.h>
#include <maya/MPlugArray.h>

#include "errorMacros.h"

#include "graph.h"
#include "jMayaIds.h"

MTypeId graph::id( k_graph );
	
MObject graph::aVectorComponent;
MObject graph::aValues;	
MObject graph::aColor;
MObject graph::aColorR;
MObject graph::aColorG;
MObject graph::aColorB;
			
MObject graph::aMaxValue;
MObject graph::aEnableGraph;		


MObject graph::aSampleBy;	
MObject graph::aHeight;	
MObject graph::aWidth;		
MObject graph::aXOffset;	
MObject graph::aYOffset;	





graph::graph() {}
graph::~graph() {}
MStatus graph::compute( const MPlug& plug, MDataBlock& data ) { return( MS::kSuccess );}

void graph::draw( M3dView & view, const MDagPath & path, 
					   M3dView::DisplayStyle style,
					   M3dView::DisplayStatus status )
{ 
	//cerr  << "in draw" << endl;
	MStatus st;
	
	
	
	MObject thisNode = thisMObject();
	
	
	
	
  	MPlug enPlug( thisNode, aEnableGraph );
	short drawStyle; 
	st = enPlug.getValue (drawStyle  );
	
	if (DrawStyle(drawStyle) == graph::kOff) {
		return;
	}
	
	
	
	float colorRed;
	float colorGreen;
	float colorBlue;
	
	MPlug colorRPlug( thisNode, aColorR );
	MPlug colorGPlug( thisNode, aColorG );
	MPlug colorBPlug( thisNode, aColorB );
	
	colorRPlug.getValue(colorRed);
	colorGPlug.getValue(colorGreen);
	colorBPlug.getValue(colorBlue);
	
	MPlug sPlug( thisNode, aSampleBy );
	short sampleBy; 
	st = sPlug.getValue (sampleBy ); mser;
	if (sampleBy < 1)  sampleBy = 1;
	

	MPlug mPlug( thisNode, aMaxValue );
	double maxValue; 
	st = mPlug.getValue (maxValue ); mser;
	if (maxValue <= 0) return;
	const int precision = 2;

	
	MPlug hPlug( thisNode, aHeight );
	double height; 
	st = hPlug.getValue (height ); mser;
	
	MPlug wPlug( thisNode, aWidth );
	double width; 
	st = wPlug.getValue (width ); mser;
	
	MPlug xPlug( thisNode, aXOffset );
	double xOffset; 
	st = xPlug.getValue (xOffset ); mser;
	
	MPlug yPlug( thisNode, aYOffset );
	double yOffset; 
	st = yPlug.getValue (yOffset ); mser;
	
	int pw = view.portWidth();
	int ph = view.portHeight();
	
	int x0, x1, y0, y1;
	
	x0 = int(xOffset * pw);
	y0 = int(yOffset * ph);
	x1 = int(((xOffset + width) * pw));
	y1 = int(((yOffset + height) * ph));
	
	if (!((x1>x0)&&(y1> y0))) return;
	
	
	

	unsigned len;
	MPlug valsPlug( thisNode, aValues );
	MObject dVals;
	st = valsPlug.getValue (dVals ); 
	if (st.error()) return;
	if (dVals.hasFn(MFn::kVectorArrayData)){
		len = MFnVectorArrayData(dVals).length();
	} else if (dVals.hasFn(MFn::kDoubleArrayData)){
		len = MFnDoubleArrayData(dVals).length();
	} else {
		return;
	}
	if (len == 0) return;
	
	
	MPlugArray plugArray;

	bool hasConnections = valsPlug.connectedTo(plugArray,1,0); 
	MString name;
	if (hasConnections) name = plugArray[0].name();
	
	// if too many values for the box, change sampleBy value
	if ((pw * width) < (len / sampleBy)) sampleBy = short((len / (pw * width)) + 1);
	int numSamples =  len / sampleBy ;
	double spacing = 1.0 / numSamples;
	double range = width * pw;

	
	double graphHeight =  ph * height ;
	double normalizer = 	graphHeight	/ maxValue;
	int i,j;
	MIntArray xPositions(numSamples);
	for ( i = 0; i< numSamples; i++){
		xPositions[i] = int(x0 + (  range * i * spacing ));
	}

	MDoubleArray values(numSamples);	
	if (dVals.hasFn(MFn::kVectorArrayData)){
		MFnVectorArrayData fnVals(dVals);
		MVectorArray arrayVals = fnVals.array();
		len =  arrayVals.length();
		
		MPlug compPlug( thisNode, aVectorComponent );
		short vc; 
		st = compPlug.getValue (vc  );
		
		if ( VectorComp(vc) == graph::kMag ) {
			for ( i=0,  j=0;  (j < numSamples) ;i+=sampleBy,j++) {
				values.set(arrayVals[i].length() , j);
			}
			name+=" (mag)";
		} else if (VectorComp(vc) == graph::kX) {
			for ( i=0,  j=0;  (j < numSamples) ;i+=sampleBy,j++) {
				values.set(arrayVals[i].x , j);
			}
			name+=" (X)";
		}else if (VectorComp(vc) == graph::kY) {
			for ( i=0,    j=0;  (j < numSamples) ;i+=sampleBy,j++) {
				values.set(arrayVals[i].y , j);
			}	
			name+=" (Y)";
		}else if (VectorComp(vc) == graph::kZ) {
			for ( i=0,    j=0;  (j < numSamples) ;i+=sampleBy,j++) {
				values.set(arrayVals[i].z , j);
			}
			name+=" (Z)";
		} else {
			return;
		}
		
	} else if (dVals.hasFn(MFn::kDoubleArrayData)){
		MFnDoubleArrayData fnVals(dVals);
		MDoubleArray arrayVals = fnVals.array();
		for ( i=0,   j=0;  (j < numSamples) ;i+=sampleBy,j++) {
			values.set(arrayVals[i] , j);
		}
	} else {
		return;
	}

	view.beginGL(); 
	
	
	glPushAttrib(GL_CURRENT_BIT);
	glClear(GL_CURRENT_BIT);
	
	
	
	
	// setup projection
	////////////////////////////
	glMatrixMode( GL_MODELVIEW );
	glPushMatrix();
	glLoadIdentity();
	glMatrixMode( GL_PROJECTION );
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(
			   0.0, (GLdouble) pw,
			   0.0, (GLdouble) ph
			   );
	glMatrixMode( GL_MODELVIEW );
	glShadeModel(GL_SMOOTH);
	////////////////////////////
	
	
	// draw the outline
	////////////////////////////
	glBegin( GL_LINE_LOOP );
	glVertex2i(x0, y0);
	glVertex2i(x0, y1);
	glVertex2i(x1, y1);
	glVertex2i(x1, y0); 
	glEnd();
	////////////////////////////
	
	
	// draw maxVal
	MString legendVal;
	legendVal.set(maxValue, precision);
	MPoint posTop,posZero,posHalf,posTopRight, farClp; 
	view.viewToWorld( x0, y1,  posTop, farClp ) ;
	view.viewToWorld( x0, y0,  posZero, farClp ) ;
	view.viewToWorld( x1, y1,  posTopRight, farClp ) ;
	posHalf = (posTop +posZero)*0.5;
	view.drawText(legendVal, posTop,  M3dView::kRight);
	view.drawText("0", posZero,  M3dView::kRight);
	legendVal.set((maxValue*0.5), precision);
	view.drawText(legendVal, posHalf,  M3dView::kRight);

	
	view.setDrawColor( MColor( MColor::kRGB, colorRed, colorGreen, colorBlue ) );
	view.drawText(name, posTopRight,  M3dView::kRight);
	
	//MIntArray xPositions;
	//MIntArray yPositions;
	
	// draw bar chart
	////////////////////////////
	glBegin(GL_LINES);
	
	
	
	for (int i = 0; i< numSamples; i++){
		glVertex2i(xPositions[i], y0);
		glVertex2i(xPositions[i],   int(y0 + (values[i] * normalizer))   )    ;	
	}

			
	glEnd();
	////////////////////////////
	
	
	glMatrixMode( GL_PROJECTION );
	glPopMatrix();
	glMatrixMode( GL_MODELVIEW );
	glPopMatrix();

		
	glPopAttrib();	
	
	view.endGL();

}

bool graph::isBounded() const
{ 
	return true;
}
MBoundingBox graph::boundingBox() const
{   
	//cerr << " in boundingBox" << endl;
	MStatus st;
	
	MObject thisNode = thisMObject();
	
  	MPlug enPlug( thisNode, aEnableGraph );
	
	
	MPlug hPlug( thisNode, aHeight );
	double height; 
	st = hPlug.getValue (height ); mser;
	
	MPlug wPlug( thisNode, aWidth );
	double width; 
	st = wPlug.getValue (width ); mser;
	
	MPlug xPlug( thisNode, aXOffset );
	double xOffset; 
	st = xPlug.getValue (xOffset ); mser;
	
	MPlug yPlug( thisNode, aYOffset );
	double yOffset; 
	st = yPlug.getValue (yOffset ); mser;
	
	M3dView view = M3dView::active3dView();
	int pw = view.portWidth();
	int ph = view.portHeight();
	
	MPoint nearPt;
	MPoint farPt;
	short scrCorner1X	=	short( xOffset * pw);
	short scrCorner1Y	=	short( yOffset * ph);
	short scrCorner2X	=	short((xOffset + width) * pw) ;
	short scrCorner2Y	=	short((yOffset + height) * ph);
	
	view.viewToWorld(scrCorner1X, scrCorner1Y, nearPt, farPt);
 	MPoint corner1((nearPt + farPt )*0.5);
	view.viewToWorld(scrCorner2X, scrCorner2Y , nearPt, farPt);
	MPoint corner2(( nearPt + farPt)*0.5);
	
	//cerr << " out boundingBox" << endl;
	
	return MBoundingBox( corner1, corner2 );
}

void* graph::creator()
{
	return new graph();
}

MStatus graph::initialize()
{ 
	
	MStatus st;
	
	
	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnCompoundAttribute cAttr;
	MFnEnumAttribute eAttr;
	MFnGenericAttribute gAttr;
	//	MFnUnitAttribute uAttr;
	
	
	aValues =  gAttr.create("values","vals",&st);mser;
	gAttr.addAccept(MFnData::kVectorArray);
	gAttr.addAccept(MFnData::kDoubleArray);
	gAttr.setStorable(false);
	gAttr.setDisconnectBehavior(MFnAttribute::kReset);
	st = addAttribute(aValues);
	
	aColorR = nAttr.create( "drawColorR", "dcr",MFnNumericData::kFloat);
    nAttr.setStorable(true);
    nAttr.setKeyable(false);
    nAttr.setConnectable(false);
    nAttr.setDefault(1.0f);
	
    aColorG = nAttr.create( "drawColorG", "dcg",MFnNumericData::kFloat);
    nAttr.setStorable(true);
    nAttr.setKeyable(false);
    nAttr.setConnectable(false);
    nAttr.setDefault(0.0f);
	
    aColorB = nAttr.create( "drawColorB", "dcb",MFnNumericData::kFloat);
    nAttr.setStorable(true);
    nAttr.setKeyable(false);
    nAttr.setConnectable(false);
	nAttr.setDefault(0.0f);
	
    aColor = nAttr.create( "drawColor", "dc", aColorR , aColorG, aColorB);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
	nAttr.setUsedAsColor(true);
	st = addAttribute(aColor);
	
	aMaxValue = nAttr.create( "maxValue", "mxv",MFnNumericData::kDouble);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    nAttr.setDefault(1.0);
	st =addAttribute(aMaxValue);
	
	aEnableGraph = eAttr.create("drawStyle","dst");
	eAttr.addField( "off"	,  graph::kOff     );
	eAttr.addField( "bars"	,  graph::kBars 	);
	eAttr.setDefault( graph::kBars );
	eAttr.setHidden( false );
	eAttr.setKeyable( true );
	st = addAttribute( aEnableGraph ); msert;
	
	aVectorComponent= eAttr.create("vectorComponent","vcm");
	eAttr.addField( "magnitude"	,  graph::kMag     );
	eAttr.addField( "xComponent"	,  graph::kX     );
	eAttr.addField( "yComponent"	,  graph::kY     );
	eAttr.addField( "zComponent"	,  graph::kZ     );
	eAttr.setDefault( graph::kMag );
	eAttr.setHidden( false );
	eAttr.setKeyable( true );
	st = addAttribute( aVectorComponent ); msert;
	
	
	aSampleBy = nAttr.create( "sampleBy", "smp",MFnNumericData::kShort);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    nAttr.setDefault(1);
	st =addAttribute(aSampleBy);
	
	aHeight = nAttr.create( "graphHeight", "gph",MFnNumericData::kDouble);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    nAttr.setDefault(0.2);
	st =addAttribute(aHeight);
	
	aWidth = nAttr.create( "graphWidth", "gpw",MFnNumericData::kDouble);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    nAttr.setDefault(0.2);
	st =addAttribute(aWidth);
	
	aXOffset = nAttr.create( "xOffset", "xof",MFnNumericData::kDouble);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    nAttr.setDefault(0.02);
	st =addAttribute(aXOffset);
	
	aYOffset = nAttr.create( "yOffset", "yof",MFnNumericData::kDouble);
    nAttr.setStorable(true);
    nAttr.setKeyable(true);
    nAttr.setDefault(0.02);
	st =addAttribute(aYOffset);
		
	return MS::kSuccess;
}

void graph::postConstructor()
{
	/*
 	MFnDependencyNode nodeFn(thisMObject());
 	nodeFn.setName("graphShape#"); 
	 */

}
