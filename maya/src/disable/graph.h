/*
 *  graph.h
 *  jtools
 *
 *  Created by Julian Mann on 28/12/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

/*
 *  graph.h
 *  animal
 *
 *  Created by Julian Mann on 30/07/2006.
 *  Copyright 2006 hooly|mama. All rights reserved.
 *
 */

#ifndef _graph_H
#define _graph_H

 #if defined(linux)
#ifndef LINUX
 #define LINUX
#endif
 #endif

#if defined(OSMac_MachO_)
//#include <OpenGL/gl.h>
//#include <OpenGL/glu.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif



#include <maya/MString.h> 
#include <maya/MTypeId.h> 
#include <maya/MPxLocatorNode.h> 
#include <maya/MDataBlock.h>
#include <maya/M3dView.h>


class graph : public MPxLocatorNode
{
public:
	graph();
	virtual ~graph(); 
	virtual void postConstructor();
	
    virtual MStatus   		compute( const MPlug& plug, MDataBlock& data );
	
	virtual void            draw( M3dView & view, const MDagPath & path, 
								  M3dView::DisplayStyle style,
								  M3dView::DisplayStatus status );
	
	virtual bool            isBounded() const;
	virtual MBoundingBox    boundingBox() const; 
	
	static  void *          creator();
	static  MStatus         initialize();
	
	
	
	static	MTypeId		id;
	
private:

	enum DrawStyle {  kOff, kBars};
	enum VectorComp {  kMag, kX, kY, kZ};

		
	static  MObject         aVectorComponent;	
	static MObject          aEnableGraph;		
	
	static  MObject         aValues;	
	static	MObject         aColor;			// graph draw color
	static	MObject         aColorR;		// graph draw color
	static	MObject         aColorG;		// graph draw color
	static	MObject         aColorB;		// graph draw color

	static MObject			aMaxValue;
	
	static	MObject         aSampleBy;		
	static	MObject         aHeight;		
	static	MObject         aWidth;		
	static	MObject         aXOffset;		
	static	MObject         aYOffset;		
	
	
	
	
};

#endif




















