#include <maya/MStatus.h>
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>
#include "errorMacros.h"

#include <maya/MDrawRegistry.h>

#include "glyphNode.h"
#include "glyphDrawOverride.h"

#include "matrixGlyphNode.h"
#include "matrixGlyphDrawOverride.h"

MStatus initializePlugin(MObject obj)
{

	MStatus st;

	MFnPlugin plugin(obj, PLUGIN_VENDOR, PLUGIN_VERSION, MAYA_VERSION);

	st = plugin.registerNode("glyphShape", glyphNode::id, glyphNode::creator,
							 glyphNode::initialize, MPxNode::kLocatorNode,
							 &glyphNode::drawDbClassification);
	msert;

	st = MHWRender::MDrawRegistry::registerDrawOverrideCreator(
		glyphNode::drawDbClassification,
		glyphNode::drawRegistrantId,
		glyphDrawOverride::Creator);
	mser;



	st = plugin.registerNode("matrixGlyphShape", matrixGlyphNode::id, matrixGlyphNode::creator,
							 matrixGlyphNode::initialize, MPxNode::kLocatorNode,
							 &matrixGlyphNode::drawDbClassification);
	msert;

	st = MHWRender::MDrawRegistry::registerDrawOverrideCreator(
		matrixGlyphNode::drawDbClassification,
		matrixGlyphNode::drawRegistrantId,
		matrixGlyphDrawOverride::Creator);
	mser;




	return st;
}

MStatus uninitializePlugin(MObject obj)
{
	MStatus st;

	MFnPlugin plugin(obj);

	st = MHWRender::MDrawRegistry::deregisterDrawOverrideCreator(
		matrixGlyphNode::drawDbClassification,
		matrixGlyphNode::drawRegistrantId);
	mser;

	st = plugin.deregisterNode(matrixGlyphNode::id);
	mser;

	st = MHWRender::MDrawRegistry::deregisterDrawOverrideCreator(
		glyphNode::drawDbClassification,
		glyphNode::drawRegistrantId);
	mser;

	st = plugin.deregisterNode(glyphNode::id);
	mser;

	return st;
}
