


#ifndef matrixGlyphDrawData_H
#define matrixGlyphDrawData_H



#include <maya/MPxDrawOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawRegistry.h>
#include <maya/MViewport2Renderer.h>
#include <maya/MMatrix.h>
#include <maya/MColor.h>
#include <maya/MPointArray.h>
#include <maya/MColorArray.h>
#include <maya/MVectorArray.h>

class matrixGlyphDrawData : public MUserData {
public:

	matrixGlyphDrawData();
	virtual ~matrixGlyphDrawData();

	MPointArray  xLines;
	MPointArray  yLines;
	MPointArray  zLines;

	MColor colorR;
	MColor colorG;
	MColor colorB;

	MVectorArray  circleNormals;
	bool doCircles;
	MColor circleColor;

	float circleScale;

	
};



#endif

