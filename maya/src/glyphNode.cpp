#include <maya/MIOStream.h>
#include <math.h>

#include <maya/MFnPluginData.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MFloatArray.h>
#include <maya/MIntArray.h>

#include <maya/MFloatPointArray.h>
#include <maya/MPoint.h>

#include <maya/MVectorArray.h>

#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnIntArrayData.h>

#include <maya/MFnNumericAttribute.h>
 

#include "glyphNode.h"
 
#include <jMayaIds.h>
#include <attrUtils.h>
 
#include "errorMacros.h"

 
MObject glyphNode::aPoints;
MObject glyphNode::aDirections;

MObject glyphNode::aNormalize;
MObject glyphNode::aLineMult;
MObject glyphNode::aPointSize;

MObject glyphNode::aPointColor;
MObject glyphNode::aLineColor;


MTypeId glyphNode::id(k_glyphNode);
MString glyphNode::drawDbClassification("drawdb/geometry/glyphNode");
MString glyphNode::drawRegistrantId("glyphNodePlugin");

glyphNode::glyphNode() {}

glyphNode::~glyphNode() {}

void *glyphNode::creator()
{
	return new glyphNode();
}

// const double epsilon = 0.0001;

MStatus glyphNode::initialize()
{
	MStatus st;
	MString method("glyphNode::initialize");

	MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;

	aPoints = tAttr.create("points", "pts", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setKeyable(true);
	st = addAttribute(aPoints);
	mser;

	aDirections = tAttr.create("directions", "dcs", MFnData::kVectorArray);
	tAttr.setStorable(false);
	tAttr.setDisconnectBehavior(MFnAttribute::kReset);
	tAttr.setKeyable(true);
	st = addAttribute(aDirections);
	mser;

	aPointSize = nAttr.create("pointSize", "psz", MFnNumericData::kFloat);
	nAttr.setDefault(2.0f);
	nAttr.setKeyable(true);
	st = addAttribute(aPointSize);
	mser;
 
 	aNormalize = nAttr.create("normalize", "nmz", MFnNumericData::kBoolean, 0, &st);
	nAttr.setKeyable(true);
	st = addAttribute(aNormalize);
	mser;

	aLineMult = nAttr.create("lineMultiplier", "mlt", MFnNumericData::kFloat);
	nAttr.setDefault(1.0f);
	nAttr.setKeyable(true);
	st = addAttribute(aLineMult);
	mser;

	aPointColor = nAttr.createColor("pointColor", "pc");
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	st = addAttribute(aPointColor);
	mser;

	aLineColor = nAttr.createColor("lineColor", "lc");
	nAttr.setStorable(true);
	nAttr.setKeyable(true);
	st = addAttribute(aLineColor);
	mser;


	return (MS::kSuccess);
}

MStatus glyphNode::compute(const MPlug &plug, MDataBlock &data)
{
	return (MS::kUnknownParameter);

}

void glyphNode::draw(M3dView &view,
					   const MDagPath &path,
					   M3dView::DisplayStyle style,
					   M3dView::DisplayStatus status)
{
}

bool glyphNode::isBounded() const
{
	return false;
}

MBoundingBox glyphNode::boundingBox() const
{
	return MBoundingBox();
}

void glyphNode::postConstructor()
{
	// setExistWithoutInConnections(true);
	// setExistWithoutOutConnections(true);
}
