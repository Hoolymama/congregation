

#ifndef glyphDrawData_H
#define glyphDrawData_H



#include <maya/MPxDrawOverride.h>
#include <maya/MUserData.h>
#include <maya/MDrawRegistry.h>
#include <maya/MViewport2Renderer.h>
#include <maya/MMatrix.h>
#include <maya/MColor.h>
#include <maya/MPointArray.h>
#include <maya/MColorArray.h>
#include <maya/MIntArray.h>

class glyphDrawData : public MUserData {
public:

	glyphDrawData();
	virtual ~glyphDrawData();

	float pointSize       ;
	bool normalize       ;
	float lineMult       ;
	
	MColor pointColor ;
	MColor lineColor ;
	
	MPointArray lineList;
	MPointArray pointList;
};



#endif
